# Simple web application for demonstrating how a Vigenere Cipher works

This application is a response to the challenge posed [here](https://github.com/florinpop17/app-ideas/blob/master/Projects/1-Beginner/Vigenere-Cipher.md), is written in javascript and SASS and leverages both React and Bootstrap.

## Running the application locally

### For Development

1. Clone this repository `git clone https://gitlab.com/kylejamesbehiels/vigenere-cipher-challenge.git`
2. `cd vigenere-cipher-challenge.git`
3. Install dependencies `npm install`
4. Run the application locally `npm start`

### For Production

1. Clone this repository `git clone https://gitlab.com/kylejamesbehiels/vigenere-cipher-challenge.git`
2. `cd vigenere-cipher-challenge.git`
3. Install dependencies `npm install`
4. Build the application `npm run build`
5. (Optional) Run the built application `npm serve`

