import React, { Component } from 'react'

export default class Home extends Component {

    constructor(){
        super();
        this.state = {
            encrypted: false,
            plaintext: "",
            encryption_key: "",
            encryption_result: "",
            hide_compare: true,
            compare_text: "Compare"
        }

        this.plaintextChanged = this.plaintextChanged.bind(this);
        this.encryptionKeyChanged = this.encryptionKeyChanged.bind(this);
        this.encryptVigenere = this.encryptVigenere.bind(this);
        this.encryptionResultChanged = this.encryptionResultChanged.bind(this);
        this.decryptVigenere = this.decryptVigenere.bind(this);
        this.toggleCompare = this.toggleCompare.bind(this);
    }

    plaintextChanged(e){
        let reg = RegExp('^([A-Z]* | \r*)*[A-Z]*$');
        if(!reg.test(e.target.value.toUpperCase())){
            alert("Please only use letters and spaces.")
            return;
        }
        let readyToEncrypt = false;
        if(this.state.encryption_key !== "" && e.target.value !== ""){
            readyToEncrypt = true;
        }
        this.setState({
            plaintext: e.target.value,
            ready_to_encrypt: readyToEncrypt
        })
    }

    encryptionKeyChanged(e){
        let reg = RegExp('^([A-Z]* | \r*)*[A-Z]*$');
        if(!reg.test(e.target.value.toUpperCase())){
            alert("Please only use letters and spaces.")
            return;
        }
        let readyToEncrypt = false;
        if(this.state.plaintext !== "" && e.target.value !== ""){
            readyToEncrypt = true;
        }
        let readyToDecrypt = false;
        if(this.state.encryption_result !== "" && e.target.value !== ""){
            readyToDecrypt = true;
        }
        this.setState({
            encryption_key: e.target.value,
            ready_to_encrypt: readyToEncrypt,
            ready_to_decrypt: readyToDecrypt
        })
    }

    encryptionResultChanged(e){
        let readyToDecrypt= false;
        if(this.state.encryption_key !== "" && e.target.value !== ""){
            readyToDecrypt = true;
        }
        this.setState({
            encryption_result: e.target.value,
            ready_to_decrypt: readyToDecrypt
        })
    }

    encryptVigenere(){
        if(this.state.encryption_result !== ""){
            if(!window.confirm("Encryption will overwrite the encrypted text field, continue?"))return;
        }

        let key = this.state.encryption_key.toUpperCase();
        let plaintext = this.state.plaintext.toUpperCase();
        let index = 0;
        let result = "";

        [...plaintext].forEach((char) => {
            if(char === " "){
                result+=" ";
                return;
            }

            let original = (char.charCodeAt(0) - 65);
            let keychar = (key.charCodeAt(index % key.length) - 65);
            result += String.fromCharCode(((original + keychar) % 26) + 65 );
            index++

        })
        this.setState({ 
            encryption_result: result,
            ready_to_decrypt: true
        })

    }


    decryptVigenere(){
        if(this.state.plaintext !== ""){
            if(!window.confirm("Decryption will overwrite the plaintext text field, continue?"))return;
        }
        let key = this.state.encryption_key.toUpperCase();
        let encrypted = this.state.encryption_result.toUpperCase();
        let index = 0;
        let result = "";

        [...encrypted].forEach((char) => {
            if(char === " "){
                result+=" ";
                return;
            }
            let original = (char.charCodeAt(0) - 65);
            let keychar = (key.charCodeAt(index % key.length) - 65);
            result += String.fromCharCode((26 + (original - keychar)) % 26 + 65 );
            index++

        })
        this.setState({ 
            plaintext: result,
            ready_to_encrypt: true
        })

    }

    toggleCompare(){
        this.setState({
            hide_compare: !this.state.hide_compare,
            compare_text: this.state.hide_compare ? "Hide Comparison" : "Compare"
        }, () => {
            document.getElementById("compare").scrollIntoView({behavior: "smooth"});
        })
    }

    render() {
        return (
            <div>
                <div className="jumbotron">
                    <div className="container">
                        <h2>Welcome to Cryptography 101!</h2>
                        <hr></hr>
                        <p>Let's try one of the simplest ciphers, the Vigenere cipher! The vigenere Cipher is second only maybe to the <a href="https://en.wikipedia.org/wiki/Caesar_cipher">Caesar Cipher</a> in terms of simplicity but managed to resist cryptanalysis from it's inception in 1553 until 1863.</p>
                        <hr></hr>
                        <table className="table table-dark">
                            <tr>
                                <th>Question</th>
                                <th>Answer</th>
                            </tr>
                            <tr>
                                <td>Would you feel confident encrypting your financial information using the Vigenere Cipher? Why?</td>
                                <td>Absolutely not. This algorithm was broken without the advent of computers. Since then advances in processing power have made this cipher all but obsolete.</td>
                            </tr>
                            <tr>
                                <td>How would you detect that a message had been encrypted using the Vigenere Cipher?</td>
                                <td>Kasiki and Friedman both used frequency analysis to determine if a cipher was encrypted with Vigenere. Given that Vigenere is just interwoven Caesar ciphers, if we are able to guess the length of the key, we can break each "individual" Caeser cipher one at a time.</td>
                            </tr>
                            <tr>
                                <td>How would you go about trying to crack this encryption?</td>
                                <td>If the cipher text is long and complex, I would use a Friedman test to determine key length. Once key length has been estimated it is trivial to break each individual Caesar cipher. Otherwise, in the case of short cipher text, it would be trivial to simply brute force the key length as it cannot be longer than the text itself.</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div className="container">
                    <div className="form">
                        <h3>Try it out!</h3>
                        <hr></hr>
                        <div className="row input-row">
                            <div className="col-6">
                                <textarea name="plaintext_input" value={this.state.plaintext} onChange={this.plaintextChanged} className="form-control" placeholder="Enter your plain-text here" rows="10"></textarea>
                            </div>
                            <div className="col-6">
                                <textarea name="result_output" value={this.state.encryption_result} onChange={this.encryptionResultChanged} className="form-control" placeholder="Enter your encrypted text here" rows="10"></textarea>
                            </div>
                        </div>
                        <div className="row input-row">
                            <div className="col-12">
                                <input name="encryption_key" value={this.state.encryption_key} onChange={this.encryptionKeyChanged} className="form-control" placeholder="Encryption Key" ></input>
                            </div>
                        </div>
                        <div className="row input-row">
                            <div className="form-btn-wrapper col-4">
                                <button className="btn btn-success form-btn" onClick={this.encryptVigenere} disabled={!this.state.ready_to_encrypt}>Encrypt</button>
                            </div>
                            <div className="form-btn-wrapper col-4">
                                <button className="btn btn-danger form-btn" onClick={this.decryptVigenere} disabled={!this.state.ready_to_decrypt}>Decrypt</button>
                            </div>
                            <div className="form-btn-wrapper col-4">
        <button className="btn btn-primary form-btn" onClick={this.toggleCompare} disabled={!this.state.ready_to_decrypt || !this.state.ready_to_encrypt}>{this.state.compare_text}</button>
                            </div>
                        </div>
                    </div>
                    <div id="compare" className={this.state.hide_compare ? "hidden" : ""}>
                        <h3>Compare</h3>
                        <hr></hr>
                        <div className="row">
                            <div className="col-6">
                                <p>
                                    {this.state.plaintext}
                                </p>
                            </div>
                            <div className="col-6">
                                <p>
                                    {this.state.encryption_result}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="spacer"></div>
            </div>
        )
    }
}
